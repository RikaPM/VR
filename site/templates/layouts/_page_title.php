<div class="page-title-cont page-title-big page-title-img bg-gray-dark" style="background-image: url(images/work-proc-bg.jpg)">
    <div class="relative container align-left">
        <div class="row">
              
            <div class="col-md-8">
                <h1 class="page-title"><?php echo $page->title() ?></h1>
                <div class="page-sub-title font-poppins">
                    <!-- More about <a href="http://getbootstrap.com/css/#grid" target="_blank">Bootstrap grid system</a> -->
                    <?php $parent_title = strtolower($page->parent()->title()) ?>
                    <?php if(strtolower($page->parent()->title()) == 'berita') : ?>
                        <?php echo date('d M Y', $page->date()) ?>
                        <span class="slash-divider">/</span>
                        <a href="<?php echo $page->parent()->url() ?>">
                            <?php echo $page->parent()->title() ?>
                        </a>
                    <?php else: ?>
                        <?php if($page->lead()) : ?>
                            <?php echo $page->lead(); ?>
                        <?php else: ?>
                            Penjelasan singkat tentang halaman <?php echo $page->title() ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            
            <?php if(strtolower($page->parent()->title()) != 'berita') : ?>
            <div class="col-md-4">
                <div class="breadcrumbs font-poppins">
                    <?php if($page->parent()->id() == null): ?>
                        <a class="a-inv" href="<?php echo $page->parent()->url() ?>">Home</a>
                    <?php else: ?>
                        <a class="a-inv" href="<?php echo $page->parent()->url() ?>"><?php echo $page->parent()->title() ?></a>
                    <?php endif; ?>
                    <span class="slash-divider">/</span>
                    <span class="bread-current"><?php echo $page->title() ?></span>
                </div>
            </div>
            <?php endif; ?>
              
        </div>
    </div>
</div>