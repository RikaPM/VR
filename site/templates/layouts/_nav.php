 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu                       
                            </button>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">

                    <?php $current_page_id = $page->id() ?>
                        <?php foreach($pages->visible() as $navitem): ?>
                            <?php if($current_page_id == $navitem->id()) : ?>
                                <?php $is_active = true; ?>
                            <?php else: ?>
                                <?php $is_active = false; ?>
                            <?php endif; ?>
                        <li <?php if($is_active == true) echo "current" ?>>
                            <a href="<?php echo $navitem->url() ?>" class="<?php if($is_active == true) echo "current" ?>">
                                <div class="main-menu-title"><?php echo $navitem->title()->html() ?></div>
                            </a>
                            
                            <?php if($navitem->hasChildren()) : ?>
                            <ul class="sub">
                                <?php foreach($navitem->children() as $subnavitem) : ?>
                                <li><a class="" href="<?php echo $subnavitem->url() ?>"><?php echo $subnavitem->title() ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>

                        </li>
                        <?php endforeach; ?>

                                <div class="clearfix"> </div>
                            </ul>   
                        </div>  