<!DOCTYPE html>
<html >    
    <head>

        <title><?php echo $site->title() ?></title>
        
        <?php include_once('_meta.php') ?>
        <?php include_once('_js.php') ?>
        <?php include_once('_css.php') ?>
        
    </head>
    <body>

          <!-- banner -->
    <div class="banner">
       <?php include_once('_top_bar.php') ?>
        <div class="header">

        <?php include_once('_header.php') ?>

    </div>
    </div>
 <?php $back1 = $site->contentURL() . "/" . $site->back_image1(); ?>
 <?php $back2 = $site->contentURL() . "/" . $site->back_image2(); ?>
 <?php $back3 = $site->contentURL() . "/" . $site->back_image3(); ?>
 <?php $arrow = $site->contentURL() . "/" . $site->top_image(); ?>     
   
    <!-- //banner -->
    <div id="kb" class="carousel kb_elastic animate_text kb_wrapper" data-ride="carousel" data-interval="6000" data-pause="hover">
            <!-- Wrapper for Slides -->
           
            <div class="carousel-inner" role="listbox">
                <!-- First Slide -->
                <div class="item active">
                    <div class="slider">
                        <img src="<?php echo $back1 ?>" alt="" />
                        <div class="carousel-caption kb_caption slider-grid">
                            <h3>Singapore</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                </div>

                <!-- Second Slide -->
                <div class="item">
                    <div class="slider slider1">
                         <img src="<?php echo $back2 ?>" alt="" />
                        <div class="carousel-caption kb_caption kb_caption_right slider-grid">
                            <h3>Hawaii</h3>
                            <p>Vivamus vel nulla venenatis, tincidunt mi vel, consequat erat.</p>
                        </div>
                    </div>
                </div>

                <!-- Third Slide -->
                <div class="item">
                    <div class="slider slider2">
                         <img src="<?php echo $back3 ?>" alt="" />
                        <div class="carousel-caption kb_caption kb_caption_center slider-grid">
                           <h3>Hong Kong</h3>
                            <p>Nunc turpis purus, vestibulum at quam ac, feugiat dignissim nunc</p>
                        </div>
                    </div>
                </div>

            </div>

           <a class="left carousel-control kb_control_left" href="#kb" role="button" data-slide="prev">
                <span class="fa fa-angle-left kb_icons" aria-hidden="true">  <img src="<?php echo $arrow ?>" alt="" /></span>
                <span class="sr-only">Previous</span>
            </a>
            <!-- Right Button -->
            <a class="right carousel-control kb_control_right" href="#kb" role="button" data-slide="next">
                <span class="fa fa-angle-right kb_icons" aria-hidden="true">  <img src="<?php echo $arrow ?>" alt="" /></span>
                <span class="sr-only">Next</span>
            </a>


           
    </div> 
    
       


       



