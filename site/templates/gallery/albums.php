<div class="row">
  <div class="col-md-12 blog-main-posts">
    <!-- TITTLE -->
<!-- <div class="heading-line h4-line mb-20">
<h4><strong>Other news</strong></h4>
</div> -->


<div class="row">



  <!-- Post Item -->
  <?php foreach($page->children() as $album) : ?>
  <div class="col-sm-6 col-md-3 wow fadeIn pb-70">
    <div class="post-prev-img iof-small-thumb">
      <a href="<?php echo $album->url() ?>">
        <?php if($image = $album->images()->sortBy('sort', 'asc')->first()): ?>
          <img src="<?php echo $image->url() ?>" alt="img" data-object-fit="cover">
        <?php endif; ?>
      </a>
    </div>

    <div class="post-prev-title">
      <h3><a href="blog-single-sidebar-right.html"><?php echo $album->title() ?></a></h3>
    </div>

    <div class="post-prev-info">
      <?php echo date('d M Y', $album->date()) ?>
      <!-- <span class="slash-divider">/</span>
      <a href="">John Doe</a>
      <span class="slash-divider">/</span>
      <a href="#">Sport</a> -->
    </div>
  </div>
  <?php endforeach; ?>

</div>

</div>
</div>
