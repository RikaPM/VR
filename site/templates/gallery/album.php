<?php $featured_image = $page->contentURL() . "/" . $page->featured_image(); ?>
<div class="page-title-large3-cont bg-gray" style="background-image: url(<?php echo $featured_image ?>); background-size: cover;">
  <div class="dark-mask">&nbsp;</div>
  <div class="container relative">
    <div class="page-title-large3-text-cont">
      <div>
        <h1 class="page-title3 font-white"><?php echo $page->title() ?></h1>
        <div class="page-sub-title text-white font-poppins">
              <i class="fa fa-map-marker"></i> <?php echo $page->location() ?>
              <span class="slash-divider">/</span>
              <i class="fa fa-calendar"></i> <?php echo date('d M Y', $page->date()) ?>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- CONTENT -->
<div class="page-section p-140-cont pb-50">
  <div class="container">

    <!-- PROJECT DETAILS & TEXT -->
    <div class="row">



      <!-- TEXT -->
      <div class="col-md-12 mb-50">
        <p class="text-highlight mb-30">
          <?php echo $page->lead() ?>
        </p>
        <p>
          <?php echo $page->text() ?>
        </p>
        <!-- <h4 class="font-open-sans"><strong>Maecenas volutpat</strong></h4> -->
        <!-- <p>Cras tellus enim, sagittis aer varius faucibus, molestie in dolor. Mauris molliadipisg elit, in vulputate est volutpat vitae. Pellentesque convallis nisl sit amet lacus luctus vel consequat ligula suscipit. Aliquam et metus sed tortor eleifend pretium non id urna. Fusce in augue leo, sed cursus nisl. Nullam vel tellus massa. Vivamus porttitor rutrum libero ac mattis. Aliquam congue malesuada mauris vitae dignissim.</p> -->
      </div>

    </div>

    <!-- IMG & DESCRIPTION -->
    <div class="row">
      <!-- ITEMS GRID -->
      <ul class="port-grid masonry clearfix" id="items-grid">


        
        <!-- Item -->
        <?php foreach($page->images() as $image) : ?>
        <li class="port-item mix design lightbox-item">
          <a href="<?php echo $image->url() ?>" class="lightbox">
            <div class="port-img-overlay">
              <img class="port-main-img" src="<?php echo $image->url() ?>" alt="img" >
            </div>
            <div class="port-overlay-cont">
              <div class="port-btn-cont">
                <div aria-hidden="true" class="icon_search"></div>
              </div>
            </div>
          </a>
        </li>
        <?php endforeach; ?>


      </ul>

    </div>


  </div><!-- container end -->

 

  <!-- TEXT & INFO -->
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-md-offset-3 mt-30">


      </div>
    </div>

    <!-- DIVIDER -->
    <hr class="mt-0 mb-0">

    <!-- WORK NAVIGATION -->
    <div class="row">
      <div class="col-md-12">
        <!-- WORK NAVIGATION -->
    <div class="work-navigation clearfix">
        
        <?php if($page->hasPrev()): ?>
        <a href="<?php echo $page->prev()->url() ?>" class="work-prev">
        <?php else: ?>
        <a class="disable work-prev">
        <?php endif; ?>
            <span>
                <span class="icon icon-arrows-left"></span>
                &nbsp;Album Sebelumnya
            </span>
        </a>

        <a href="<?php echo $page->parent()->url() ?>" class="work-all"><span>All Posts</span></a>
        
        <?php if($page->hasNext()): ?>
        <a href="<?php echo $page->next()->url() ?>" class="work-next">
        <?php else: ?>
        <a class="disable work-next">
        <?php endif; ?>
            <span>
                Album Berikutnya&nbsp;
                <span class="icon icon-arrows-right"></span>
            </span>
        </a>

    </div>
      </div>
    </div>

    <!-- DIVIDER -->
    <hr class="mt-0 mb-0">

  </div><!-- container end -->

</div><!-- page section end