<style>
    figure {
        margin-bottom: 40px;
    }
</style>

<div class="container p-140-cont">
    <div class="row">
          
        <!-- SIDEBAR -->
        <?php if($page->hasChildren()) : ?>
        <div class="col-md-4">
            <h4 class="font-20 mt-0">Submenu <?php echo $page->title() ?></h4>
            <ul id="nav-sidebar" class="nav bs-sidenav blog-categories font-poppins">
                <?php foreach($page->children()->visible() as $subpage) : ?>
                <li>
                    <a href="<?php echo $subpage->url() ?>">
                        <span class="blog-cat-icon">
                            <i class="fa fa-angle-right"></i>
                        </span>
                        <?php echo $subpage->title() ?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>
    
        
        <?php if(($page->hasNext() or $page->hasPrevious()) and $page->parent()->id() != ""): ?>
        <?php //if(false): ?>
        <div class="col-md-4">
            <h4 class="font-20 mt-0">Menu <?php echo $page->parent()->title() ?></h4>
            <ul id="nav-sidebar" class="nav bs-sidenav blog-categories font-poppins">
                <?php foreach($page->siblings() as $siblings) : ?>
                <li class="<?php if($siblings->id() == $page->id()) echo "current" ?>">
                    <a href="<?php echo $siblings->url() ?>">
                        <span class="blog-cat-icon">
                            <i class="fa fa-angle-right"></i>
                        </span>
                        <?php echo $siblings->title() ?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>
            
        <!-- CONTENT -->
        <div class="col-md-8">
            <div class="heading-underline">
                <h3 class="mt-0 mb-50"><?php echo $page->title() ?></h3>
            </div>
        
            <?php if($page->featured_image()) : ?>
            <figure>
                <img src="<?php echo $page->contentURL() . "/" . $page->featured_image() ?>" alt="<?php echo $page->title()->html() ?>">
            </figure>
            <?php endif; ?>
            <p>
                <?php echo $page->text()->kirbytext() ?>
            </p>
        </div>

          </div>
              
        </div>