<!-- DIVIDER -->
<hr class="mt-0 mb-0">
                
    <!-- WORK NAVIGATION -->
    <div class="work-navigation clearfix">
        
        <?php if($page->hasPrev()): ?>
        <a href="<?php echo $page->prev()->url() ?>" class="work-prev">
        <?php else: ?>
        <a class="disable work-prev">
        <?php endif; ?>
            <span>
                <span class="icon icon-arrows-left"></span>
                &nbsp;Prev
            </span>
        </a>

        <a href="<?php echo $page->parent()->url() ?>" class="work-all"><span>All Posts</span></a>
        
        <?php if($page->hasNext()): ?>
        <a href="<?php echo $page->next()->url() ?>" class="work-next">
        <?php else: ?>
        <a class="disable work-next">
        <?php endif; ?>
            <span>
                Next&nbsp;
                <span class="icon icon-arrows-right"></span>
            </span>
        </a>

    </div>
                  
<!-- DIVIDER -->
<hr class="mt-0 mb-0">