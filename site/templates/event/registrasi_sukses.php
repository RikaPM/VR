<div class="container p-140-cont">
        <div class="row">
            <div class="col-md-12"> 
                <div class="alert alert-success">
                    <?php echo $page->text(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo $site->url() ?>" class="button thin full-rounded medium gray" href="#">
                    <i class="fa fa-long-arrow-left"></i> Kembali ke Home
                </a>
                <a href="<?php echo $site->url() ?>/informasi/kegiatan" class="button full-rounded medium gray" href="#">
                    Lihat event lainnya
                </a>
            </div>
        </div>
</div>