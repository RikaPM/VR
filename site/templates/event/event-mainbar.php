<div class="col-sm-12 blog-main-posts">

    <?php $i = 0; ?>
    <?php foreach($page->children()->limit(5) as $event): ?>

    <?php if($i%2 == 0): ?>
        <div class="row">
    <?php endif; ?>

    <div class="col-md-6 wow fadeIn pb-90">

        <div class="post-prev-img iof-big-thumb">
            <?php if($image = $event->images()->sortBy('sort', 'asc')->first()): ?>
                <a href="<?php echo $event->url() ?>">
                    <img src="<?php echo $image->url() ?>" alt="img" data-object-fit="cover">
                </a>
            <?php endif; ?>
        </div>

        <div class="post-prev-title">
            <h3 class="post-title-big">
                <a href="<?php echo $event->url() ?>">
                    <?php echo $event->title() ?>
                </a>
            </h3>
        </div>

        <div class="post-prev-info ">
            <?php echo date('d M Y', $event->date()) ?>
            <?php echo $event->time() ?>
            <span class="slash-divider">/</span>
            <a href=""><?php echo $event->location() ?></a>
            <span class="slash-divider">/</span>
            <?php echo $event->price() ?>
        </div>

        <div class="mb-30">
            <?php echo $event->text()->excerpt(200) ?>
        </div>

        <div class="post-prev-more-cont clearfix">
            <div class="post-prev-more left">
                <a href="<?php echo $event->url() ?>" class="font-poppins button rounded medium gray">Lihat Detail</a>
            </div>
        </div>
    </div>

    <?php if($i%2 == 1): ?>
        </div>
    <?php endif; ?>
    <?php $i++; ?>
    <?php endforeach; ?>
</div>
