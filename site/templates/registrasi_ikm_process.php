<?php

$data = $_POST;
$ikmData = array(
    'id_ikm' => $data['id_ikm'],
    'badan_usaha' => $data['badan_usaha'],
    'nama_perusahaan' => $data['nama_perusahaan'],
    'nama_pemilik' => $data['nama_pemilik'],
    'alamat' => $data['alamat'],
    'desa_kelurahan' => $data['desa_kelurahan'],
    'kecamatan' => $data['kecamatan'],
    'telepon' => $data['telepon'],
    'fax' => $data['fax'],
    'email' => $data['email'],
    'website' => $data['website'],
    'ijin_ui' => $data['ijin_ui'],
    'tahun_ijin_ui' => $data['tahun_ijin_ui'],
    'kbli_kode' => $data['kbli_kode'],
    'nama_produk' => $data['nama_produk'],
    'jenis_produk' => $data['jenis_produk'],
    'tahun_data' => $data['tahun_data'],
    'tk_laki' => $data['tk_laki'],
    'tk_perempuan' => $data['tk_perempuan'],
    'nilai_investasi' => $data['nilai_invetasi'],
    'kapasitas_produksi' => $data['kapasitas_produksi'],
    'satuan' => $data['satuan'],
    'nilai_produksi' => $data['nilai_produksi'],
    'nilai_bb' => $data['nilai_bb'],
    'pemasaran_ekspor' => $data['pemasaran_ekspor'],
    'negara_ekspor' => $data['negara_ekspor'],
    'status' => '0',
    'flag' => '1',
    'create_date' => date("Y-m-d H:i:s"),
    'create_by' => 'registran',
    'mod_date' => date("Y-m-d H:i:s"),
    'mod_by' => 'registran',
);
$result = db::insert('ikm', $ikmData);

// if($result) {
header("Location: " . $site->url() . "/registrasi/ikm/sukses");
// }

?>