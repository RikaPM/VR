<?php include_once( 'layouts/_head.php' ) ?>
<?php include_once( 'layouts/_page_title.php' ) ?>

<div class="container p-140-cont pt-xxs-80">

<?php
$dataIkm = db::select('ikm', '*');
// print_r($dataIkm);
?>

<p class="grey-label">Filter berdasarkan :</p>
<div id="ext-filter-produk" class="custom-filter"></div>
<div id="ext-filter-daerah" class="custom-filter"></div>

<div class="table-responsive">
    <table id="ikmTable" class="table table-condensed mb-40" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Nama Perusahaan</th>
                <th>Nama Pemilik</th>
                <th>Jenis Produk</th>
                <th>Kecamatan</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Nama Perusahaan</th>
                <th>Nama Pemilik</th>
                <th>Jenis Produk</th>
                <th>Kecamatan</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach($dataIkm as $ikm) : ?>
            <tr>
                <td><?php echo $ikm->nama_perusahaan; ?></td>
                <td><?php echo $ikm->nama_pemilik ?></td>
                <td><?php echo $ikm->nama_produk ?></td>
                <td><?php echo $ikm->kecamatan ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>


</div>
<?php include_once( 'layouts/_foot.php' ) ?>
