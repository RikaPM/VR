<div id="about" class="page-section bg-yellow" data-scroll-index="1">
    <div class="container fes1-cont pb-0">

        <div class="row">
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        <div class="fes1-main-title-cont wow fadeInDown">
                            <div class="fes1-title-50 font-poppins">
                                <strong class="font-signpainter">
                                    Selamat Datang di
                                    <br>
                                    Website Resmi Disperindag
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>

                     <div class="row">

                    <div class="col-md-12">
                        <div class="lightbox-item">
                            <a class="button thin full-rounded medium gray" href="<?php echo $site->video_profil() ?>" class="popup-youtube">
                                <i class="fa fa-play"></i> Lihat Video Profil Disperindag
                            </a>
                          </div>
                    </div>
                    </div>

                    <br>
                    <br>
                <div class="row">

                    <?php foreach(page('profil')->children() as $profil): ?>

                    <a href="<?php echo $profil->url() ?>" class="welcome col-md-6 col-sm-6">
                        <div class="fes1-box wow fadeIn" >
                            <div class="fes1-box-icon">
                                <?php if($profil->icon() != "") : ?>
                                <div class="fa fa-<?php echo $profil->icon() ?>"></div>
                                <?php else: ?>
                                <div class="fa fa-star-o"></div>
                                <?php endif; ?>
                            </div>
                            <h3><?php echo $profil->title()->html() ?></h3>
                        </div>
                    </a>

                    <?php endforeach; ?>






                </div> <!-- row -->
    <br>
    <br>

            </div>

            <div class="col-md-4 mt-30 fes1-img-cont wow fadeInUp">
                <?php $figure_img = $site->contentURL() . "/" . $site->figure_image(); ?>
                <img src="<?php echo $figure_img ?>" alt="img" class="wow fadeInUp" data-wow-delay="150ms" data-wow-duration="1s">
                <!-- <img src="images/fes11-2.jpg" alt="img" > -->
            </div>

        </div>
    </div>
</div>
