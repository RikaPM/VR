<div class="page-section pt-110-b-30-cont">
          <div class="container">
                
            <div class="mb-50">
              <h2 class="section-title2 pr-0">Latest News<a href="blog-right-sidebar.html" class="section-more right">All News</a>
                  </h2>
            </div>
            
            <div class="row">
              
              <!-- Post Item 1 -->
              <div class="col-md-12 wow fadeIn pb-30" >
                <div class="row">
                
                  <div class="col-md-4 blog2-post-title-cont">
                    <div class="post-prev-title">
                      <div class="post-prev-info">
                        <a href="#">design</a><span class="slash-divider">/</span><a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">john doe</a>
                      </div>
                      <h3><a href="blog-single-sidebar-right.html">Time For Minimalism</a></h3>
                    </div>
                  </div>
                    
                  <div class="col-md-8">
                    <div class="blog2-post-prev-text">                     
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, excepturi modi cupiditate ipsam molestiae eveniet incidunt nostrum atque alias dicta? Quos, quisquam iusto numquam dicta vel voluptatem aperiam voluptas similique!                      
                    </div>
                  </div>
                  
                </div>
              </div>
              
              <!-- Post Item 2 -->
              <div class="col-md-12 wow fadeIn pb-30" >
                <div class="row">
                
                  <div class="col-md-4 blog2-post-title-cont">
                    <div class="post-prev-date-cont">
                      <span class="blog2-date-numb">17</span><span class="blog2-month">August</span>
                    </div>
                    <div class="post-prev-title">
                      <h3><a href="blog-single-sidebar-right.html">New Trends In Web</a></h3>
                      <div class="post-prev-info">
                        <a href="#">design</a><span class="slash-divider">/</span><a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">john doe</a>
                      </div>
                    </div>
                  </div>
                    
                  <div class="col-md-8">
                    <div class="blog2-post-prev-text">                     
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, excepturi modi cupiditate ipsam molestiae eveniet incidunt nostrum atque alias dicta? Quos, quisquam iusto numquam dicta vel voluptatem aperiam voluptas similique!                      
                    </div>
                  </div>
                  
                </div>
              </div>

              <!-- Post Item 3 -->
              <div class="col-md-12 wow fadeIn pb-30" >
                <div class="row">
                
                  <div class="col-md-4 blog2-post-title-cont">
                    <div class="post-prev-date-cont">
                      <span class="blog2-date-numb">1</span><span class="blog2-month">August</span>
                    </div>
                    <div class="post-prev-title">
                      <h3><a href="blog-single-sidebar-right.html">The Sound Of Life</a></h3>
                      <div class="post-prev-info">
                        <a href="#">design</a><span class="slash-divider">/</span><a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">john doe</a>
                      </div>
                    </div>
                  </div>
                    
                  <div class="col-md-8">
                    <div class="blog2-post-prev-text">                     
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, excepturi modi cupiditate ipsam molestiae eveniet incidunt nostrum atque alias dicta? Quos, quisquam iusto numquam dicta vel voluptatem aperiam voluptas similique!                      
                    </div>
                  </div>
                  
                </div>
              </div>

            </div>
            
          </div>
        </div>