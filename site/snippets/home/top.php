



        <div class="about">
        <div class="container">
            <div class="w3l-about-heading">
                <h2><?php echo $site->title() ?></h2>
                <p><?php echo $site->text() ?></P>
            </div>
                <div class="about-grids">
            <?php foreach(page('top')->children()->find('top')->children()->limit(4)->visible() as $top): ?> 


               <div class="col-md-3 about-grid">
              <div class="about-grid-info effect-1">
                   <?php if($image = $top->images()->sortBy('sort', 'asc')->first()): ?>
                        <img src="<?php echo $image->url() ?>" alt="" data-object-fit="cover" >
                         <?php endif; ?>
                        <h4><?php echo $top->title()->html() ?></h4>  
                    </div>
                </div>    


         <?php endforeach; ?></div><div class="clearfix"> </div>
    </div>

