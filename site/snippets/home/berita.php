<!-- TESTIMONIALS 4 -->
<div class="page-section p-130-cont bg-white" data-scroll-index="2">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="mb-70">
                    <h2 class="section-title2 font-light font-signpainter text-center p-0">Berita dan Pengumuman Disperindag</h2>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12 wow fadeIn">

                <div class="col-md-12">
                    <h4 class="pb-20">
                        Berita Terbaru
                        <a href="<?php echo $site->url() ?>/informasi/berita/" class="section-more right">Lihat Berita Lainnya</a>
                    </h4>
                </div>

                <?php foreach(page('informasi')->children()->find('berita')->children()->limit(4)->visible() as $berita): ?>

                <!-- Post Item 1 -->
                <div class="col-sm-6 col-md-3 col-lg-3 wow fadeIn pb-70" >
                    <div class="post-prev-img iof-small-thumb">
                        <a href="<?php echo $site->url() ?>/informasi/berita">
                            <?php if($image = $berita->images()->sortBy('sort', 'asc')->first()): ?>
                            <img src="<?php echo $image->url() ?>" alt="" data-object-fit="cover">
                        <?php endif; ?>
                        <!-- <img src="images/blog/blog-sect3-post-anim.gif" alt="img"> -->
                    </a>
                </div>
                <div class="post-prev-title">
                    <h3>
                        <a href="<?php echo $berita->url() ?>"><?php echo $berita->title()->html() ?></a>
                    </h3>
                </div>
                <div class="post-prev-info">
                    <?php echo date('d M Y', $berita->date()) ?>
                    <span class="slash-divider">/</span>
                    <a href="<?php echo $berita->parent()->url() ?>">Berita</a>
                </div>
                <!-- <div> -->
                <?php //echo $berita->text()->excerpt(100) ?>
                <!-- </div> -->
            </div>

        <?php endforeach; ?>
    </div>

    </div>
    <div class="row">
    <div class="col-md-6 wow fadeIn">
        <div class="col-md-12">
            <h4 class="pb-20">
                News in Picture
                <a href="<?php echo $site->url() ?>/galeri/foto/" class="section-more right">Lihat Galeri Berita</a>
            </h4>
        </div>
        <div class="col-md-12">
            <ul class="port-grid masonry clearfix" id="items-grid">

                <!-- Item big -->
                <?php foreach(page('galeri/foto')->children() as $album) : ?>
                <li class="port-item mix design lightbox-item">
                    <a href="<?php echo $album->url() ?>" class="lightbox">
                        <div class="port-img-overlay">
                            <?php if($image = $album->images()->sortBy('sort', 'asc')->first()): ?>
          <img class="port-main-img" src="<?php echo $image->url() ?>" alt="img" data-object-fit="cover">
        <?php endif; ?>
                        </div>
                        <div class="port-overlay-cont">
                            <div class="port-btn-cont">
                                <div aria-hidden="true" class="icon_search"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <?php endforeach; ?>


            </ul>
        </div>
    </div>

    <div class="col-md-6 wow fadeIn">
        <div class="col-md-12">
            <h4 class="pb-20">
                Pengumuman Terbaru
                <a href="<?php echo $site->url() ?>/informasi/pengumuman/" class="section-more right">Lihat Pengumuman Lainnya</a>
            </h4>
        </div>
        <div class="col-md-12 widget-body">
                  <ul class="clearlist widget-posts">
                    <?php foreach(page('informasi')->children()->find('pengumuman')->children()->limit(2)->visible() as $pengumuman): ?>
                    <li class="clearfix">
                      <a href="#">
                        <img style="width: 70px" src="<?php echo $site->contentURL() ?>/<?php echo $site->default_image_square() ?>" alt="" class="widget-posts-img"></a>
                      <div class="widget-posts-descr2">
                        <a href="<?php echo $pengumuman->url() ?>" class="font-poppins lh-18"><?php echo $pengumuman->title() ?></a>
                        <div class="lh-18"><?php echo date('d M Y', $pengumuman->date()) ?></div>
                      </div>
                    </li>
                    <?php endforeach; ?>


                  </ul>
                </div>

    </div>


</div>
<!-- <div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning">Pengumuman</div>
    </div>
</div> -->
</div>
</div>
