<!-- CLIENTS 2 -->
        <div class="page-section p-80-cont partner-logo">
                    <div class="container">
         <!-- TITLE -->
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title2 text-center mb-45 p-0 font-signpainter">Partner Disperindag</h2>
            </div>
        </div>
            <div class="row align-center">

              <?php foreach(page('link-partner')->children() as $partner): ?>
                    <?php if($image = $partner->images()->sortBy('sort', 'asc')->first()): ?>
                        <!-- <div class="col-xs-6 col-sm-2 client2-item"> -->
                          <img style="height: 100px" alt="<?php echo $page->title() ?>" src="<?php echo $image->url() ?>">
                        <!-- </div> -->
                    <?php endif; ?>
                <?php endforeach; ?>


            </div>

          </div>
                </div>
