<!-- FEATURES 12 HALF COLORED -->
<div class="page-section">
  <div class="container-fluid">
    <div data-equal=".equal-height" class="row row-sm-fix">
      
      <?php foreach(page('aplikasi-produk')->children()->limit(2) as $produk): ?>
      <?php
      $hex = $produk->color();
      list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
      $color = "rgba($r, $g, $b, 0.5)";
      ?>
      <?php $featured_image = $produk->contentURL() . "/" . $produk->featured_image() ?>
      <div class="app-box col-md-6 fes12-img equal-height" style="background-image: url(<?php echo $featured_image ?>)">
        <div class="dark-mask" style="background-color: <?php echo $color ?>;"></div>
        <div class="fes2-main-text-cont text-white">
          <div class="fes2-title-45 font-poppins text-white">
            <strong><?php echo $produk->title() ?></strong>
          </div>
          <div class="fes2-text-cont"><?php echo $produk->lead() ?></div>
          <div class="fes12-btn-cont mt-30">
            <?php if(false): ?>
            <a class="button medium dark rounded thin btn-4 btn-4cc" href="<?php echo $produk->url() ?>">
              <span class="button-text-anim">Lihat Detail</span>
              <span aria-hidden="true" class="button-icon-anim arrow_right"></span>
            </a>
            <?php endif; ?>
            <?php if($produk->link() != "") : ?>
            <a class="button medium white rounded thin btn-4 btn-4cc" href="<?php echo $produk->link() ?>">
              <span class="button-text-anim">Link ke Produk</span>
              <span aria-hidden="true" class="button-icon-anim arrow_right"></span>
            </a>
            <?php endif; ?>
          </div>
        </div>
      </div>
    <?php endforeach; ?>


    </div>
  </div>
</div>