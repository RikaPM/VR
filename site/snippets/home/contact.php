

        <div id="contact-link" class="page-section p-100-cont bg-gray" data-scroll-index="7">
          <div class="container">

            <div class="row">
            <div class="col-md-12">
                <h2 class="section-title2 text-center mb-45 p-0 font-signpainter">Kontak Disperindag</h2>
            </div>
        </div>

            <div class="row">

            <div class="col-md-4">

            <div class="col-md-12 col-sm-6">
                <div class="cis-cont">
                  <div class="cis-icon">
                    <div class="icon icon-basic-home"></div>
                  </div>
                  <div class="cis-text">
                    <h3>Kantor</h3>
                    <p class="fes14-tab-text"><?php echo $site->gedung() ?></p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-6">
                <div class="cis-cont">
                  <div class="cis-icon">
                    <div class="icon icon-basic-geolocalize-05"></div>
                  </div>
                  <div class="cis-text">
                    <h3>Alamat</h3>
                    <p class="fes14-tab-text"><?php echo $site->alamat() ?></p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-6">
                <div class="cis-cont">
                  <div class="cis-icon">
                    <div class="icon icon-basic-mail"></div>
                  </div>
                  <div class="cis-text">
                    <h3>Email</h3>
                    <p><a href="mailto:info@elementy.com"><?php echo $site->email() ?></a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-6">
                <div class="cis-cont">
                  <div class="cis-icon">
                    <div class="icon icon-basic-smartphone"></div>
                  </div>
                  <div class="cis-text">
                    <h3>Hubungi Kami</h3>
                    <p class="fes14-tab-text">
                      <?php if($site->telp() != "") : ?>
                      <?php echo $site->telp() ?> (Kantor)
                      <?php endif; ?>
                      <?php if($site->mobile() != "") : ?>
                      <br>
                      <?php echo $site->mobile() ?> (Mobile)
                      <?php endif; ?>
                      <?php if($site->whatsapp() != "") : ?>
                      <br>
                      <?php echo $site->whatsapp() ?> (WhatsApp)</p>
                      <?php endif; ?>
                  </div>
                </div>
              </div>
              </div>
              <div class="col-md-8">
                <?php echo $site->googlemap()->html() ?>
              </div>
            </div>
          </div>
        </div>
