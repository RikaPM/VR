<div id="blog-link" class="page-section pt-150-b-110-cont bg-gray" data-scroll-index="3">
    <div class="container">

        <div class="row">

      <div class="col-md-12">
        <div class="mb-70">
          <h2 class="section-title2 font-light font-signpainter text-center p-0">Kegiatan dan Agenda Disperindag</h2>
      </div>
  </div>

</div>



        <div class="row">



            <!-- Post Item 1 -->
            <div class="col-sm-12 col-md-6 col-lg-6 wow fadeIn" >

                <div class="col-md-12">
        <h4 class="pb-20">
            Kegiatan Disperindag
            <a href="<?php echo $site->url() ?>/informasi/kegiatan/" class="section-more right">Lihat Kegiatan Lainnya</a>
        </h4>
    </div>
<?php
?>
                <div class="col-md-12 wow fadeIn">
                    <?php foreach(page('informasi')->children()->find('kegiatan')->children()->limit(1)->visible() as $kegiatan): ?>
                <div class="post-prev-img">
                    <a href="<?php echo $kegiatan->url() ?>">
                            <?php if($image = $kegiatan->images()->sortBy('sort', 'asc')->first()): ?>
                                <img src="<?php echo $image->url() ?>" alt="">
                            <?php endif; ?>
                        </a>
                </div>

                <div class="post-prev-title">
                  <h3 class="post-title-big">
                    <a href="<?php echo $kegiatan->url() ?>"><?php echo $kegiatan->title() ?></a>
                    <!-- <a href="blog-single-sidebar-right.html">New trends in web design</a> -->
                </h3>
                </div>

                <div class="post-prev-info">
                                <?php echo date('d M Y', $kegiatan->date()) ?>
                                <!-- <span class="slash-divider">/</span> -->
                                <a href=""><?php //echo $kegiatan->location() ?></a>
                            </div>


                <div class="mb-30">
                    <?php echo $kegiatan->text()->excerpt(100) ?> <a href="<?php echo $kegiatan->url() ?>">Baca selengkapnya</a>
                </div>
            <?php endforeach; ?>


              </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6 wow fadeIn pb-70" >
                <div class="col-md-12">
        <h4 class="pb-20">
            Kalender Agenda Disperindag
            <!-- <a href="" class="section-more right">Lihat Kegiatan Lainnya</a> -->
        </h4>
    </div>
                <div class="col-md-12 bs-example accord-2">
                                <div class="panel-group accord-chev" >

                                    <?php foreach(page('informasi')->children()->find('agenda')->children()->limit(5)->visible() as $agenda): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo md5($agenda->id()) ?>">
                                                    <?php echo date('d M Y', $agenda->date()) ?> <strong><?php echo $agenda->title() ?></strong></a>
                                            </h4>
                                        </div>
                                        <div id="<?php echo md5($agenda->id()) ?>" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p><?php echo $agenda->lead() ?> <br> <a href="">Baca Selengkapnya</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>


                                </div>
                            </div>
            </div>


        </div> <!-- row -->

    </div> <!-- container -->
</div> <!-- page-section -->
