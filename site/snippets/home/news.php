    <div class="news">
        <div class="container">
            <div class="news-heading">
                <h3>News & Events</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras porta efficitur ante quis volutpat.</p>
            </div>
              <?php $figure_img1 = $site->contentURL() . "/" . $site->figure_image1(); ?>
              <?php $figure_img2 = $site->contentURL() . "/" . $site->figure_image2(); ?>
              <?php $figure_img3 = $site->contentURL() . "/" . $site->figure_image3(); ?>
            

           

            <div class="news-grids">
                <div class="col-md-4 news-grid">
                    <div class="agile-news-grid-info">
                        <div class="news-grid-info-img">
                            <a href="single.html"><img src="<?php echo $figure_img1 ?>" alt="img" /></a>
                        </div>
                        <div class="news-grid-info-bottom">
                            <div class="date-grid">
                                <div class="admin">
                                    <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Admin</a>
                                </div>
                                <div class="time">
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> May 09,2016</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="news-grid-info-bottom-text">
                                <a href="single.html">Quisque gravida, nunc eu interdum porta</a>
                                <p>Aliquam erat volutpat. Duis vulputate tempus laoreet. Integer viverra eleifend neque, eget dictum lectus. Quisque eu tempor dolor.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 news-grid">
                    <div class="agile-news-grid-info">
                        <div class="news-grid-info-img">
                            <a href="single.html"><img src="<?php echo $figure_img2 ?>" alt="" /></a>
                        </div>
                        <div class="news-grid-info-bottom">
                            <div class="date-grid">
                                <div class="admin">
                                    <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Admin</a>
                                </div>
                                <div class="time">
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> May 09,2016</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="news-grid-info-bottom-text">
                                <a href="single.html">Quisque gravida, nunc eu interdum porta</a>
                                <p>Aliquam erat volutpat. Duis vulputate tempus laoreet. Integer viverra eleifend neque, eget dictum lectus. Quisque eu tempor dolor.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 news-grid">
                    <div class="agile-news-grid-info">
                        <div class="news-grid-info-img">
                            <a href="single.html"><img src="<?php echo $figure_img3 ?>" alt="" /></a>
                        </div>
                        <div class="news-grid-info-bottom">
                            <div class="date-grid">
                                <div class="admin">
                                    <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Admin</a>
                                </div>
                                <div class="time">
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> May 09,2016</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="news-grid-info-bottom-text">
                                <a href="single.html">Quisque gravida, nunc eu interdum porta</a>
                                <p>Aliquam erat volutpat. Duis vulputate tempus laoreet. Integer viverra eleifend neque, eget dictum lectus. Quisque eu tempor dolor.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>