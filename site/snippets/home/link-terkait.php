<!-- CLIENTS 2 -->
          <div class="page-section">
            <div class="container">
              <div class="col-md-12">
          <div class="divider divider-center"><i class="fa fa-circle"></i></div>
              </div>
            </div>
          </div>


        <div class="page-section p-80-cont partner-logo" data-scroll-index="5">
                    <div class="container">
         <!-- TITLE -->
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title2 text-center mb-45 p-0 font-signpainter">Link Terkait</h2>
            </div>
        </div>
            <div class="row align-center">

              <?php foreach(page('aplikasi-produk')->children()->visible() as $partner): ?>
                    <?php if($image = $partner->images()->sortBy('sort', 'asc')->first()): ?>
                        <!-- <div class="col-xs-6 col-sm-2 client2-item"> -->
                        <a href="<?php echo $partner->link() ?>">
                          <img style="height: 100px" alt="<?php echo $page->title() ?>" src="<?php echo $image->url() ?>">
                        </a>
                        <!-- </div> -->
                    <?php endif; ?>
                <?php endforeach; ?>


            </div>

          </div>
                </div>
