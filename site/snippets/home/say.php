<!-- TESTIMONIALS 4 -->
<div class="page-section p-130-cont bg-white" data-scroll-index="4">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <div class="mb-70">
          <h2 class="section-title2 font-light font-signpainter text-center p-0">Lebih dekat dengan Disperindag</h2>
        </div>
      </div>

    </div>
    <div class="row">

      <div class="col-sm-6">

        <div class="col-md-12">
        <h4 class="pb-20">
            <i class="fa fa-comments"></i> Pengaduan dan Komunikasi Online
            <!-- <a href="<?php echo $site->url() ?>/informasi/kegiatan/" class="section-more right">Lihat Kegiatan Lainnya</a> -->
        </h4>
    </div>

        <div class="col-md-12" id="disqus_thread"></div>
                <script>
                    var disqus_config = function () {
                    this.page.url = '<?php echo $page->url() ?>';
                    this.page.identifier = '<?php echo $page->id() ?>';
                    };

                    (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');

                    s.src = '//disperindagmlg.disqus.com/embed.js';

                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>
                    Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a>
                </noscript>
      </div>

       <div class="col-sm-6">

        <div class="col-md-12">
        <h4 class="pb-20">
            <i class="fa fa-facebook"></i> Facebook Disperindag
            <!-- <a href="<?php echo $site->url() ?>/informasi/kegiatan/" class="section-more right">Lihat Kegiatan Lainnya</a> -->
        </h4>
        </div>

        <!-- <div class="col-md-12 mb-30">
          <div class="ts4-text-cont facebook-bg">
            Ini contoh teks untuk posting facebook di wall milik Facebook Disperindag
          </div>
          <div class="ts4-author-cont clearfix">
            <div class="ts4-author-info">
              <div class="ts-name">Anggrean Renozonarca</div>
              <div class="ts-type">Facebook</div>
            </div>
          </div>
        </div> -->
        <div class="col-md-12">
          <!-- <div class="alert alert-warning">Please wait..</div> -->
          <div class="fb-page" data-href="https://www.facebook.com/DisperindagKotaMalang/" data-tabs="timeline" data-width="500" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/DisperindagKotaMalang/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/DisperindagKotaMalang/">Disperindag Kota Malang</a></blockquote></div>
        </div>

          <div class="col-md-12">
        <h4 class="pb-20">
            <i class="fa fa-twitter"></i> Twitter Disperindag
            <!-- <a href="<?php echo $site->url() ?>/informasi/kegiatan/" class="section-more right">Lihat Kegiatan Lainnya</a> -->
        </h4>
        </div>

       <!--  <div class="col-md-12 mb-30">
          <div class="ts4-text-cont twitter-bg">
            Ini adalah contoh tweet yang mention @disperindagmalang
          </div>
          <div class="ts4-author-cont clearfix">
            <div class="ts4-author-info">
              <div class="ts-name">Yuri Pratama</div>
              <div class="ts-type">Twitter</div>
            </div>
          </div>
        </div> -->

        <div class="col-md-12">
          <!-- <div class="alert alert-warning">Please wait..</div> -->
          <a class="twitter-timeline" data-theme="light" href="https://twitter.com/Disperindag_Mlg">Tweets by Disperindag_Mlg</a>
        </div>
      </div>

    </div


  </div>
</div>
</div>
