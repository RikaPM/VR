<div class="page-section blog-sect3 bg-gray">
    <div class="container p-140-cont ">
          
        <!-- TITLE -->
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title2 text-center mb-45 p-0 font-signpainter">Kegiatan Terbaru</h2>
            </div>
        </div>
            
        <!-- BG GRAY -->
        <div class="bg-white clearfix">
            
            <!-- BLOG ROW -->
            <?php $i = 0; ?>
            <?php foreach(page('informasi')->children()->find('kegiatan')->children()->limit(2) as $kegiatan): ?>

            <div class="row">
                
            <?php if($i%2 == 0) : ?>
                <div class="col-md-6 pr-0">
            <?php else: ?>
                <div class="col-md-6 pos-l-md-50pc pl-0">
            <?php endif; ?>
                    <div class="post2-prev-img">
                        <a href="<?php echo $kegiatan->url() ?>">
                            <?php if($image = $kegiatan->images()->sortBy('sort', 'asc')->first()): ?>
                                <img src="<?php echo $image->url() ?>" alt="">
                            <?php endif; ?>
                        </a>
                    </div>
                </div>
            
            <?php if($i%2 == 0) : ?>
                <div class="col-md-6 pl-0">
            <?php else: ?>
                <div class="col-md-6 pos-r-md-50pc pr-0">
            <?php endif; ?>
                    <div class="blog-sect3-text-cont">
                        <div class="pos-v-center">
                            <div class="post2-prev-title">
                                <h3>
                                    <a href="<?php echo $kegiatan->url() ?>"><?php echo $kegiatan->title() ?></a>
                                </h3>
                            </div>
                            <div class="post-prev-info">
                                <?php echo date('d M Y', $kegiatan->date()) ?>
                                <span class="slash-divider">/</span>
                                <a href=""><?php echo $kegiatan->location() ?></a>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div> <!-- row -->
            <?php $i++; ?>
            <?php endforeach; ?>

        </div>
            
        <!-- VIEW ALL -->
        <div class="row">
            <div class="col-md-12 blog-sect3-view-all-cont">
                <a href="<?php echo $site->url() ?>/informasi/kegiatan" class="font-poppins"><strong>lihat seluruh kegiatan</strong></a>
            </div>
        </div>
              
    </div>
</div>