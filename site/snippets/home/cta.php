<!-- <div class="page-section bg-g-b-grad">
    <div class="container">
        <div class="video-ads-text-cont p-130-cont clearfix">
            <div class="section-title2 font-white font-light p-0">Anda Memiliki IKM?</div>
            <p>Ayo segera datakan IKM milik Anda sekarang! Gratis dan tidak dipungut biaya!</p>
            <div>
                <a class="hover-op-70 pb-0" href="http://themeforest.net/user/abcgomel/portfolio" title="app store"><img src="images/content/app-store.png" alt="app store" ></a><a class="hover-op-70 pb-0" href="http://themeforest.net/user/abcgomel/portfolio" title="android store"><img src="images/content/android-market.png" alt="android store" ></a>
            </div>
        </div>
    </div>
</div> -->
<?php $cta_img = $site->contentURL() . "/" . $site->cta_image(); ?>
<div data-scroll-index="6" id="blockquotes2" class="page-section p-130-cont bg-g-b-grad hidden-xs" style="background-image: url(<?php echo $cta_img ?>); background-size: 100%; background-position: bottom center; background-repeat: no-repeat;background-color:#444;">
    <div class="container">
        <blockquote class="bq2-cont text-center ls-1 font-20 pb-0 text-white">
            <div class="section-title2 font-white font-light p-0">Anda Memiliki IKM?</div>
            <br>
          Ayo segera datakan IKM milik Anda sekarang! Gratis dan tidak dipungut biaya!
            <br>
          <br>
          <a class="cta button thin full-rounded large white" href="<?php $site->url() ?>/registrasi/ikm">Daftar Sekarang!</a>
          <footer class="ls-norm font-18">
              <a href="<?php $site->url() ?>/registrasi/ikm" style="text-decoration: underline">Apa keuntungan menjadi anggota IKM Disperindag?</a>
          </footer>
        </blockquote>
    </div>
</div>

<div id="blockquotesMobile" class="page-section p-130-cont padding hidden-sm hidden-md hidden-lg" style="background-image: url('assets/images/malang-join-us-mobile.jpg'); background-size: 100%; background-position: bottom center; background-repeat: no-repeat;background-color:#444;">
    <div class="container">
        <blockquote class="bq2-cont text-center ls-1 font-20 pb-0 text-white">
            <div class="section-title2 font-white font-light p-0">Anda Memiliki IKM?</div>
            <br>
          Ayo segera datakan IKM milik Anda sekarang! Gratis dan tidak dipungut biaya!
            <br>
          <br>
          <a class="cta button thin full-rounded large white" href="<?php $site->url() ?>/registrasi/ikm">Daftar Sekarang!</a>
          <footer class="ls-norm font-18">
              <a href="<?php $site->url() ?>/registrasi/ikm" style="text-decoration: underline">Apa keuntungan menjadi anggota IKM Disperindag?</a>
          </footer>
        </blockquote>
    </div>
</div>
