<div class="hero-mobile hidden-md hidden-lg" style="height:100vh;overflow:hidden;">
        <div style="background-image:url('assets/images/malang-creative-land-mobile.jpg');background-size: 100%; background-position: bottom center;background-repeat: no-repeat;background-color:#f7f1c1;height:100vh;"></div>
</div>
<!-- SLIDER Revo Hero 4 -->
        <div class="relative hidden-xs hidden-sm" data-scroll-index="0">

            <div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_280_1_wrapper" style="margin:0px auto;background-color:#f8f0c2;padding:0px;margin-top:0px;margin-bottom:0px;padding-top:10px;">
                <!-- START REVOLUTION SLIDER 5.1.4 fullwidth mode -->
                <div class="rev_slider fullwidthabanner" data-version="5.1.4" id="rev_slider_280_1" style="display:none;">
                    <ul>

                        <!-- SLIDE  -->
                        <li data-index="rs-673" data-transition="zoomout" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-description="">
                            <!-- MAIN IMAGE -->
                            <!-- <?php //$hero_img = $site->contentURL() . "/" . $site->hero_image(); ?> -->
                            <!-- <img style="width: 100%" src="<?php echo $hero_img ?>" alt="" data-bgposition="top center" data-bgfit="cover" data-width="100%" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina> -->

                            <!-- MAIN IMAGE -->
                            <img alt="img" class="rev-slidebg" data-bgparallax="2" data-bgposition="bottom center" data-bgfit="cover" data-width="100%" data-bgrepeat="no-repeat" data-ease="Linear.easeNone" data-no-retina src="assets/images/malang-bg.jpg">

                            <!-- LAYERS -->

                            <!-- LAYER TUGU -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-1"
                                data-x="['center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['110','110','110','110']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:bottom;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2500"
                                data-responsive_offset="on"
                                style="z-index: 5;">
                                <img src="assets/images/malang-tugu.png" alt="" width="400" height="530" data-ww="['200px','200px','200px','200px']" data-hh="['265px','265px','265px','265px']" data-no-retina>
                            </div>

                            <!-- LAYER BALKOT -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-2"
                                data-x="['right','right','right','right']"
                                data-hoffset="['-140','-140','-140','-140']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['119','110','131','131']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2800"
                                data-responsive_offset="on"
                                style="z-index: 4;">
                                <img src="assets/images/malang-balkot.png" alt="" width="944" height="246" data-ww="['472px','472px','472px','472px']" data-hh="['123px','123px','123px','123px']" data-no-retina>
                            </div>

                            <!-- LAYER STASIUN -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-3"
                                data-x="['middle','middle','middle','middle']"
                                data-hoffset="['150','150','150','150']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['119','110','131','131']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2800"
                                data-responsive_offset="on"
                                style="z-index: 3;">
                                <img src="assets/images/malang-stasiun.png" alt="" width="634" height="206" data-ww="['317px','317px','317px','317px']" data-hh="['103px','103px','103px','103px']" data-no-retina>
                            </div>

                            <!-- LAYER TOKO LEGENDARIS -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-4"
                                data-x="['middle','middle','middle','middle']"
                                data-hoffset="['-110','-110','-110','-110']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['119','110','131','131']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:left;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2800"
                                data-responsive_offset="on"
                                style="z-index: 4;">
                                <img src="assets/images/malang-tokolegendaris.png" alt="" width="456" height="190" data-ww="['228px','228px','228px','228px']" data-hh="['95px','95px','95px','95px']" data-no-retina>
                            </div>

                            <!-- LAYER TANDON -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-5"
                                data-x="['left','left','left','left']"
                                data-hoffset="['200','200','200','200']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['119','110','131','131']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:left;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2800"
                                data-responsive_offset="on"
                                style="z-index: 4;">
                                <img src="assets/images/malang-tandon.png" alt="" width="480" height="368" data-ww="['240px','240px','240px','240px']" data-hh="['184px','184px','184px','184px']" data-no-retina>
                            </div>

                            <!-- LAYER ALUN ALUN TEXT -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-6"
                                data-x="['left','left','left','left']"
                                data-hoffset="['250','250','250','250']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['180','180','180','180']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:left;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2800"
                                data-responsive_offset="on"
                                style="z-index: 3;">
                                <img src="assets/images/malang-alunalun.png" alt="" width="596" height="52" data-ww="['298px','298px','298px','298px']" data-hh="['26px','26px','26px','26px']" data-no-retina>
                            </div>

                            <!-- LAYER WORKERS -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-7"
                                data-x="['right','right','right','right']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['50','50','50','50']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="3000"
                                data-responsive_offset="on"
                                style="z-index: 6;">
                                <img src="assets/images/malang-workers.png" alt="" width="408" height="176" data-ww="['204px','204px','204px','204px']" data-hh="['88px','88px','88px','88px']" data-no-retina>
                            </div>

                            <!-- LAYER ROBOT -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-8"
                                data-x="['right','right','right','right']"
                                data-hoffset="['200','200','200','200']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['65','65','65','65']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="3100"
                                data-responsive_offset="on"
                                style="z-index: 4;">
                                <img src="assets/images/malang-robot.png" alt="" width="166" height="146" data-ww="['83','83','83','83']" data-hh="['73','73','73','73']" data-no-retina>
                            </div>

                            <!-- LAYER STARTUP -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-9"
                                data-x="['middle','middle','middle','middle']"
                                data-hoffset="['250','250','250','250']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['0','0','0','0']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="3300"
                                data-responsive_offset="on"
                                style="z-index: 8;">
                                <img src="assets/images/malang-startup.png" alt="" width="522" height="348" data-ww="['261','261','261','261']" data-hh="['174','174','174','174']" data-no-retina>
                            </div>

                            <!-- LAYER SUTRADARA -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-10"
                                data-x="['middle','middle','middle','middle']"
                                data-hoffset="['50','50','50','50']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['20','20','20','20']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="3500"
                                data-responsive_offset="on"
                                style="z-index: 7;">
                                <img src="assets/images/malang-sutradara.png" alt="" width="234" height="272" data-ww="['117','117','117','117']" data-hh="['136','136','136','136']" data-no-retina>
                            </div>

                            <!-- LAYER TRAKTOR -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-11"
                                data-x="['left','left','left','left']"
                                data-hoffset="['250','250','250','250']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['35','35','35','35']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:left;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="3800"
                                data-responsive_offset="on"
                                style="z-index: 7;">
                                <img src="assets/images/malang-traktor.png" alt="" width="512" height="216" data-ww="['256','256','256','256']" data-hh="['108','108','108','108']" data-no-retina>
                            </div>

                            <!-- LAYER BAKSO -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-12"
                                data-x="['left','left','left','left']"
                                data-hoffset="['-95','-95','-95','-95']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['0','0','0','0']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:left;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2800"
                                data-responsive_offset="on"
                                style="z-index: 7;">
                                <img src="assets/images/malang-bakso.png" alt="" width="740" height="408" data-ww="['370','370','370','370']" data-hh="['204','204','204','204']" data-no-retina>
                            </div>

                            <!-- LAYER BOTANI -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-13"
                                data-x="['middle','middle','middle','middle']"
                                data-hoffset="['-250','-250','-250','-250']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['0','0','0','0']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:left;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="4000"
                                data-responsive_offset="on"
                                style="z-index: 7;">
                                <img src="assets/images/malang-botani.png" alt="" width="128" height="280" data-ww="['64','64','64','64']" data-hh="['140','140','140','140']" data-no-retina>
                            </div>

                            <!-- LAYER BUNGA -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-14"
                                data-x="['middle','middle','middle','middle']"
                                data-hoffset="['-120','-120','-120','-120']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['0','0','0','0']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:left;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="4200"
                                data-responsive_offset="on"
                                style="z-index: 7;">
                                <img src="assets/images/malang-bunga.png" alt="" width="146" height="242" data-ww="['73','73','73','73']" data-hh="['121','121','121','121']" data-no-retina>
                            </div>

                            <!-- LAYER GAMER -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-15"
                                data-x="['right','right','right','right']"
                                data-hoffset="['-15','-15','-15','-15']"
                                data-y="['bottom','bottom','bottom','bottom']"
                                data-voffset="['0','0','0','0']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="4200"
                                data-responsive_offset="on"
                                style="z-index: 7;">
                                <img src="assets/images/malang-gamer.png" alt="" width="230" height="264" data-ww="['115','115','115','115']" data-hh="['132','132','132','132']" data-no-retina>
                            </div>

                            <!-- LAYER CLOUD F -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-16"
                                data-x="['center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['-35','-35','-35','-35']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:top;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2300"
                                data-responsive_offset="on"
                                style="z-index: 2;">
                                <div class="rs-looped rs-slideloop"
                                    data-easing="Power1.easeOut"
                                    data-xs="-100"
                                    data-ys="0"
                                    data-xe="100"
                                    data-ye="0"
                                    data-speed="15">
                                    <img src="assets/images/malang-cloud-f.png" alt="" width="2656" height="192" data-ww="['1336','1336','1336','1336']" data-hh="['96','96','96','96']" data-no-retina>
                              </div>
                            </div>

                            <!-- LAYER CLOUD B -->
                            <div class="tp-caption tp-resizeme" id="slide-673-layer-17"
                                data-x="['center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['-75','-75','-75','-75']"
                                data-width="none" data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:top;s:1500;e:Power3.easeOut;"
                                data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;"
                                data-start="2400"
                                data-responsive_offset="on"
                                style="z-index: 1;">
                                <div class="rs-looped rs-slideloop"
                                    data-easing="Power1.easeOut"
                                    data-xs="150"
                                    data-ys="0"
                                    data-xe="-150"
                                    data-ye="0"
                                    data-speed="14">
                                    <img src="assets/images/malang-cloud-b.png" alt="" width="2492" height="220" data-ww="['1246','1246','1246','1246']" data-hh="['110','110','110','110']" data-no-retina>
                              </div>
                            </div>

                        </li>

                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->

            <div class="local-scroll-cont">
                <a href="#" class="scroll-down smooth-scroll" data-scroll-goto="1">
                	<div class="icon icon-arrows-down"></div>
                </a>
          	</div>
        </div>
